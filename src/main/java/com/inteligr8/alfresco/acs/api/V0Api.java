/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.acs.api;

import java.util.List;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

import com.inteligr8.alfresco.acs.model.v0.AssociationInfo;
import com.inteligr8.alfresco.acs.model.v0.ClassInfo;
import com.inteligr8.alfresco.acs.model.v0.MimeTypesData;
import com.inteligr8.alfresco.acs.model.v0.PropertyInfo;
import com.inteligr8.alfresco.acs.model.v0.ServerData;

@Path("/service/api")
public interface V0Api {

    @GET
    @Path("/server")
    @Produces({ "application/json" })
    public ServerData getServer();

    @GET
    @Path("/mimetypes/descriptions")
    @Produces({ "application/json" })
    public MimeTypesData getMimeTypes();

    @GET
    @Path("/defclasses")
    @Produces({ "application/json" })
    public List<ClassInfo> getClasses(
    		@QueryParam("cf") String classFilter,
    		@QueryParam("nsp") String namespacePrefix,
    		@QueryParam("n") String name);

    @GET
    @Path("/defclasses/{classPrefix}/{classLocalName}")
    @Produces({ "application/json" })
    public ClassInfo getClass(
    		@PathParam("classPrefix") String classPrefix,
    		@PathParam("classLocalName") String classLocalName);

    @GET
    @Path("/defclasses/{classPrefix}/{classLocalName}/subclasses")
    @Produces({ "application/json" })
    public List<ClassInfo> getClassSubclasses(
    		@PathParam("classPrefix") String classPrefix,
    		@PathParam("classLocalName") String classLocalName);

    @GET
    @Path("/defclasses/{classPrefix}/{classLocalName}/associations")
    @Produces({ "application/json" })
    public List<AssociationInfo> getClassAssociations(
    		@PathParam("classPrefix") String classPrefix,
    		@PathParam("classLocalName") String classLocalName);

    @GET
    @Path("/defclasses/{classPrefix}/{classLocalName}/association/{assocPrefix}/{assocLocalName}")
    @Produces({ "application/json" })
    public AssociationInfo getClassAssociation(
    		@PathParam("classPrefix") String classPrefix,
    		@PathParam("classLocalName") String classLocalName,
    		@PathParam("assocPrefix") String assocPrefix,
    		@PathParam("assocLocalName") String assocLocalName);

    @GET
    @Path("/defclasses/{classPrefix}/{classLocalName}/properties")
    @Produces({ "application/json" })
    public List<PropertyInfo> getClassProperties(
    		@PathParam("classPrefix") String classPrefix,
    		@PathParam("classLocalName") String classLocalName);

    @GET
    @Path("/defclasses/{classPrefix}/{classLocalName}/property/{propPrefix}/{propLocalName}")
    @Produces({ "application/json" })
    public PropertyInfo getClassProperty(
    		@PathParam("classPrefix") String classPrefix,
    		@PathParam("classLocalName") String classLocalName,
    		@PathParam("propPrefix") String propPrefix,
    		@PathParam("propLocalName") String propLocalName);
    
    @GET
    @Path("/properties")
    @Produces({ "application/json" })
    public List<PropertyInfo> getProperties(
    		@QueryParam("nsp") String namespacePrefix,
    		@QueryParam("name") String name,
    		@QueryParam("type") String dataType);

}
