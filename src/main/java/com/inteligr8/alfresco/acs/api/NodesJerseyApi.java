/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.acs.api;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import com.inteligr8.alfresco.acs.model.Error;
import com.inteligr8.alfresco.acs.model.NodeBodyCreateMultipartJersey;
import com.inteligr8.alfresco.acs.model.NodeEntry;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/api/-default-/public/alfresco/versions/1")
@Api(value = "/api/-default-/public/alfresco/versions/1", description = "")
public interface NodesJerseyApi {

    @POST
    @Path("/nodes/{nodeId}/children")
    @Consumes({ MediaType.MULTIPART_FORM_DATA })
    @Produces({ MediaType.APPLICATION_JSON })
    @ApiOperation(value = "Create a node", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Successful response", response = NodeEntry.class),
        @ApiResponse(code = 400, message = "Invalid parameter: **nodeId** is not a valid format or **nodeBodyCreate** is invalid "),
        @ApiResponse(code = 401, message = "Authentication failed"),
        @ApiResponse(code = 403, message = "Current user does not have permission to create children of **nodeId**"),
        @ApiResponse(code = 404, message = "**nodeId** or **renditionId** does not exist "),
        @ApiResponse(code = 409, message = "New name clashes with an existing node in the current parent folder"),
        @ApiResponse(code = 413, message = "Content exceeds individual file size limit configured for the network or system"),
        @ApiResponse(code = 415, message = "Content Type is not supported"),
        @ApiResponse(code = 422, message = "Model integrity exception including a file name containing invalid characters"),
        @ApiResponse(code = 507, message = "Content exceeds overall storage quota limit configured for the network or system"),
        @ApiResponse(code = 200, message = "Unexpected error", response = Error.class) })
    public NodeEntry createNode(
    		@PathParam("nodeId") String nodeId,
    		NodeBodyCreateMultipartJersey file);

}
