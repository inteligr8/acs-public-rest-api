/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.acs.model;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jakarta.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A Jersey utility implementation of the Jersey FormDataMultiPart.
 * 
 * @author brian@inteligr8.com
 */
public class NodeBodyCreateMultipartJersey extends FormDataMultiPart {

	private static final Logger logger = LoggerFactory.getLogger(NodeBodyCreateMultipartJersey.class);
	private static final ObjectMapper om = new ObjectMapper();
	
	public static NodeBodyCreateMultipartJersey from(
			NodeBodyCreate nodeBody, String filename, InputStream istream,
			Boolean autoRename, Boolean majorVersion, Boolean versioningEnabled) throws IOException, ParseException {
		NodeBodyCreateMultipartJersey multipart = new NodeBodyCreateMultipartJersey();
		multipart.field("autoRename", String.valueOf(autoRename))
				.field("majorVersion", String.valueOf(majorVersion))
				.field("versioningEnabled", String.valueOf(versioningEnabled))
				.bodyPart(toBodyPart(filename, istream))
				.getBodyParts().addAll(toFields(nodeBody));
		return multipart;
	}
 	
	private NodeBodyCreateMultipartJersey() throws IOException {
	}
	
	public NodeBodyCreate getBody() throws IOException {
		BodyPart bodyPart = this.getField("");
		if (bodyPart == null)
			throw new IllegalStateException();
		if (!MediaType.APPLICATION_JSON_TYPE.equals(bodyPart.getMediaType()))
			throw new IllegalStateException();
		
		InputStream istream = bodyPart.getEntityAs(InputStream.class);
		try {
			return om.readValue(istream, NodeBodyCreate.class);
		} finally {
			istream.close();
		}
	}
	
	public FormDataBodyPart getFiledataAttachment() {
		return this.getField("filedata");
	}
	
	
	
	private static List<FormDataBodyPart> toFields(NodeBodyCreate nodeBody) throws IOException {
		List<FormDataBodyPart> fields = new LinkedList<>();
		fields.add(new FormDataBodyPart("name", nodeBody.getName()));
		fields.add(new FormDataBodyPart("nodeType", nodeBody.getNodeType()));
		if (nodeBody.getAspectNames() != null && !nodeBody.getAspectNames().isEmpty())
			logger.warn("The ACS Public REST API does not support the explicit inclusion of aspects while creating content");
		if (nodeBody.getProperties() != null) {
			@SuppressWarnings("unchecked")
            Map<String, ?> props = (Map<String, ?>)nodeBody.getProperties();
			for (Entry<String, ?> prop : props.entrySet()) {
                if (prop.getValue() instanceof Collection<?>) {
                    for (Object value : (Collection<?>)prop.getValue())
                        if (value != null)
                            fields.add(new FormDataBodyPart(prop.getKey(), value.toString()));
                } else if (prop.getValue() instanceof Object[]) {
                    for (Object value : (Object[])prop.getValue())
                        if (value != null)
                            fields.add(new FormDataBodyPart(prop.getKey(), value.toString()));
                } else if (prop.getValue() != null) {
                    // FIXME convert dates as ACS would expect them to be formatted
                    fields.add(new FormDataBodyPart(prop.getKey(), prop.getValue().toString()));
                }
			}
		}
		
		return fields;
	}

	private static BodyPart toBodyPart(String filename, InputStream istream) throws ParseException {
		if (filename == null) {
			return new FormDataBodyPart()
					.contentDisposition(new FormDataContentDisposition("form-data; name=\"filedata\""))
					.entity(istream);
		} else {
			return new FormDataBodyPart()
					.contentDisposition(new FormDataContentDisposition("form-data; name=\"filedata\"; filename=\"" + filename + "\""))
					.entity(istream);
		}
	}

}
