/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.acs.model.v0;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClassInfo extends NamedObject {
	
	@JsonProperty
	private boolean isAspect;

	@JsonProperty
	private boolean isContainer;

	@JsonProperty
	private String description;

	@JsonProperty
	private ParentInfo parent;

	@JsonProperty
	private Map<String, AspectInfo> defaultAspects;

	@JsonProperty
	private Map<String, PropertyInfo> properties;

	@JsonProperty
	private Map<String, AssociationInfo> associations;

	@JsonProperty("childassociations")
	private Map<String, AssociationInfo> childAssociations;
	
	
	private Object syncAssocs = new Object();
	private boolean correctedAssocs = false;
	
	private Object syncChildAssocs = new Object();
	private boolean correctedChildAssocs = false;
	
	
	public boolean isAspect() {
		return isAspect;
	}

	public void setAspect(boolean isAspect) {
		this.isAspect = isAspect;
	}

	public boolean isContainer() {
		return isContainer;
	}

	public void setContainer(boolean isContainer) {
		this.isContainer = isContainer;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ParentInfo getParent() {
		return parent;
	}

	public void setParent(ParentInfo parent) {
		this.parent = parent;
	}

	public Map<String, AspectInfo> getDefaultAspects() {
		return defaultAspects;
	}

	public void setDefaultAspects(Map<String, AspectInfo> defaultAspects) {
		this.defaultAspects = defaultAspects;
	}

	public Map<String, PropertyInfo> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, PropertyInfo> properties) {
		this.properties = properties;
	}

	public Map<String, AssociationInfo> getAssociations() {
		synchronized (this.syncAssocs) {
			if (!this.correctedAssocs)
				for (AssociationInfo assoc : this.associations.values())
					assoc.setChildAssociation(false);
			this.correctedAssocs = true;
		}

		return associations;
	}

	public void setAssociations(Map<String, AssociationInfo> associations) {
		synchronized (this.syncAssocs) {
			this.associations = associations;
			this.correctedAssocs = false;
		}
	}

	public Map<String, AssociationInfo> getChildAssociations() {
		synchronized (this.syncChildAssocs) {
			if (!this.correctedChildAssocs)
				for (AssociationInfo assoc : this.childAssociations.values())
					assoc.setChildAssociation(false);
			this.correctedChildAssocs = true;
		}
		
		return childAssociations;
	}

	public void setChildAssociations(Map<String, AssociationInfo> childAssociations) {
		synchronized (this.syncChildAssocs) {
			this.childAssociations = childAssociations;
			this.correctedChildAssocs = false;
		}
	}
	
	

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class ParentInfo extends NamedObject {}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class AspectInfo extends NamedObject {}

}
