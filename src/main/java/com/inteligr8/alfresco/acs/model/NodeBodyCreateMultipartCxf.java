/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.acs.model;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jakarta.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A CXF utility implementation of the CXF MultipartBody.
 * 
 * @author brian@inteligr8.com
 */
public class NodeBodyCreateMultipartCxf extends MultipartBody {
	
	private static final Logger logger = LoggerFactory.getLogger(NodeBodyCreateMultipartCxf.class);
	private static final ObjectMapper om = new ObjectMapper();
	
	public static NodeBodyCreateMultipartCxf from(
			NodeBodyCreate nodeBody, String filename, InputStream istream,
			Boolean autoRename, Boolean majorVersion, Boolean versioningEnabled) throws IOException {
		List<Attachment> atts = new LinkedList<>();
		atts.addAll(toAttachments(nodeBody));
		if (autoRename != null)
			atts.add(toAttachment("autoRename", String.valueOf(autoRename)));
		if (majorVersion != null)
			atts.add(toAttachment("majorVersion", String.valueOf(majorVersion)));
		if (versioningEnabled != null)
			atts.add(toAttachment("versioningEnabled", String.valueOf(versioningEnabled)));
		atts.add(toAttachment(filename, istream));
		return new NodeBodyCreateMultipartCxf(atts, true);
	}
 	
	public NodeBodyCreateMultipartCxf(List<Attachment> atts) throws IOException {
		super(atts);
	}
 	
	public NodeBodyCreateMultipartCxf(List<Attachment> atts, boolean outbound) throws IOException {
		super(atts, outbound);
	}
	
	public NodeBodyCreate getBody() throws IOException {
		if (!MediaType.APPLICATION_JSON_TYPE.equals(this.getRootAttachment().getContentType()))
			throw new IllegalStateException();
		
		InputStream istream = this.getRootAttachment().getDataHandler().getInputStream();
		try {
			return om.readValue(istream, NodeBodyCreate.class);
		} finally {
			istream.close();
		}
	}
	
	public Attachment getFiledataAttachment() {
		return this.getAttachment("filedata");
	}
	
	
	
	private static List<Attachment> toAttachments(NodeBodyCreate nodeBody) throws IOException {
		List<Attachment> atts = new LinkedList<>();
		atts.add(toAttachment("name", nodeBody.getName()));
		atts.add(toAttachment("nodeType", nodeBody.getNodeType()));
		if (nodeBody.getAspectNames() != null && !nodeBody.getAspectNames().isEmpty())
			logger.warn("The ACS Public REST API does not support the explicit inclusion of aspects while creating content");
		if (nodeBody.getProperties() != null) {
			@SuppressWarnings("unchecked")
            Map<String, ?> props = (Map<String, ?>)nodeBody.getProperties();
			for (Entry<String, ?> prop : props.entrySet()) {
				if (prop.getValue() instanceof Collection<?>) {
				    for (Object value : (Collection<?>)prop.getValue())
				        if (value != null)
				            atts.add(toAttachment(prop.getKey(), value.toString()));
				} else if (prop.getValue() instanceof Object[]) {
                    for (Object value : (Object[])prop.getValue())
                        if (value != null)
                            atts.add(toAttachment(prop.getKey(), value.toString()));
				} else if (prop.getValue() != null) {
					// FIXME convert dates as ACS would expect them to be formatted
					atts.add(toAttachment(prop.getKey(), prop.getValue().toString()));
				}
			}
		}
		return atts;
	}
	
	private static Attachment toAttachment(String name, String value) {
		return new Attachment(name, new ByteArrayInputStream(value.getBytes()), new ContentDisposition("form-data; name=\"" + name + "\""));
	}

	private static Attachment toAttachment(String filename, InputStream istream) {
		if (filename == null) {
			return new Attachment("filedata", istream, new ContentDisposition("form-data; name=\"filedata\""));
		} else {
			return new Attachment("filedata", istream, new ContentDisposition("form-data; name=\"filedata\"; filename=\"" + filename + "\""));
		}
	}

}
