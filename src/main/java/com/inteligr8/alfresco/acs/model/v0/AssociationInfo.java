/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.acs.model.v0;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AssociationInfo extends NamedObject {
	
	@JsonProperty("isChildAssociation")
	private boolean childAssociation;
	
	@JsonProperty("protected")
	private boolean protected_;
	
	@JsonProperty
	private boolean duplicateChildNameAllowed;

	@JsonProperty
	private EndpointInfo source;

	@JsonProperty
	private EndpointInfo target;
	
	
	
	public boolean isChildAssociation() {
		return childAssociation;
	}
	
	public void setChildAssociation(boolean childAssociation) {
		this.childAssociation = childAssociation;
	}
	
	public boolean isProtected() {
		return protected_;
	}
	
	public void setProtected(boolean protected_) {
		this.protected_ = protected_;
	}
	
	public boolean isDuplicateChildNameAllowed() {
		return duplicateChildNameAllowed;
	}
	
	public void setDuplicateChildNameAllowed(boolean duplicateChildNameAllowed) {
		this.duplicateChildNameAllowed = duplicateChildNameAllowed;
	}
	
	public EndpointInfo getSource() {
		return source;
	}

	public void setSource(EndpointInfo source) {
		this.source = source;
	}

	public EndpointInfo getTarget() {
		return target;
	}

	public void setTarget(EndpointInfo target) {
		this.target = target;
	}
	
	

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class EndpointInfo {
		
		@JsonProperty("class")
		private String className;
		
		@JsonProperty
		private boolean mandatory;

		@JsonProperty
		private boolean many;

		public String getClassName() {
			return className;
		}

		public void setClassName(String className) {
			this.className = className;
		}

		public boolean isMandatory() {
			return mandatory;
		}

		public void setMandatory(boolean mandatory) {
			this.mandatory = mandatory;
		}

		public boolean isMany() {
			return many;
		}

		public void setMany(boolean many) {
			this.many = many;
		}
		
	}

}
