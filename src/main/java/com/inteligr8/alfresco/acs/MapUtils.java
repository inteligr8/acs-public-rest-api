/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.acs;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapUtils {
	
	public static Map<String, String> toStringMap(Map<String, ?> map) {
		if (map == null)
			return null;
		
		Map<String, String> strmap = new HashMap<>(map.size());
		for (Entry<String, ?> element : map.entrySet()) {
			String strvalue = element.getValue() == null ? null : element.getValue().toString();
			strmap.put(element.getKey(), strvalue);
		}
		
		return strmap;
	}

}
