/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.acs;

import com.inteligr8.alfresco.acs.api.ActionsApi;
import com.inteligr8.alfresco.acs.api.ActivitiesApi;
import com.inteligr8.alfresco.acs.api.AspectsApi;
import com.inteligr8.alfresco.acs.api.AuditApi;
import com.inteligr8.alfresco.acs.api.AuthenticationApi;
import com.inteligr8.alfresco.acs.api.CommentsApi;
import com.inteligr8.alfresco.acs.api.DeploymentsApi;
import com.inteligr8.alfresco.acs.api.DiscoveryApi;
import com.inteligr8.alfresco.acs.api.DownloadsApi;
import com.inteligr8.alfresco.acs.api.FavoritesApi;
import com.inteligr8.alfresco.acs.api.GroupsApi;
import com.inteligr8.alfresco.acs.api.NetworksApi;
import com.inteligr8.alfresco.acs.api.NodesApi;
import com.inteligr8.alfresco.acs.api.PeopleApi;
import com.inteligr8.alfresco.acs.api.PreferencesApi;
import com.inteligr8.alfresco.acs.api.ProbesApi;
import com.inteligr8.alfresco.acs.api.ProcessDefinitionsApi;
import com.inteligr8.alfresco.acs.api.ProcessesApi;
import com.inteligr8.alfresco.acs.api.QueriesApi;
import com.inteligr8.alfresco.acs.api.RatingsApi;
import com.inteligr8.alfresco.acs.api.RenditionsApi;
import com.inteligr8.alfresco.acs.api.SearchApi;
import com.inteligr8.alfresco.acs.api.SharedLinksApi;
import com.inteligr8.alfresco.acs.api.SitesApi;
import com.inteligr8.alfresco.acs.api.TagsApi;
import com.inteligr8.alfresco.acs.api.TasksApi;
import com.inteligr8.alfresco.acs.api.TrashcanApi;
import com.inteligr8.alfresco.acs.api.TypesApi;
import com.inteligr8.alfresco.acs.api.V0Api;
import com.inteligr8.alfresco.acs.api.VersionsApi;

/**
 * This interface consolidates the JAX-RS APIs available in the ACS Public
 * ReST API.
 * 
 * @author brian@inteligr8.com
 */
public interface AcsPublicRestApi {
	
	<T> T getApi(Class<T> apiClass);
	
	default ActionsApi getActionsApi() {
		return this.getApi(ActionsApi.class);
	}
	
	default ActivitiesApi getActivitiesApi() {
		return this.getApi(ActivitiesApi.class);
	}
	
	default AspectsApi getAspectsApi() {
		return this.getApi(AspectsApi.class);
	}

	default AuditApi getAuditApi() {
		return this.getApi(AuditApi.class);
	}
	
	default AuthenticationApi getAuthenticationApi() {
		return this.getApi(AuthenticationApi.class);
	}
	
	default CommentsApi getCommentsApi() {
		return this.getApi(CommentsApi.class);
	}
    
    default DeploymentsApi getDeploymentsApi() {
        return this.getApi(DeploymentsApi.class);
    }
	
	default DiscoveryApi getDiscoveryApi() {
		return this.getApi(DiscoveryApi.class);
	}
	
	default DownloadsApi getDownloadsApi() {
		return this.getApi(DownloadsApi.class);
	}
	
	default FavoritesApi getFavoritesApi() {
		return this.getApi(FavoritesApi.class);
	}
	
	default GroupsApi getGroupsApi() {
		return this.getApi(GroupsApi.class);
	}
	
	default NetworksApi getNetworksApi() {
		return this.getApi(NetworksApi.class);
	}
	
	default NodesApi getNodesApi() {
		return this.getApi(NodesApi.class);
	}
	
	default PeopleApi getPeopleApi() {
		return this.getApi(PeopleApi.class);
	}
    
    default PreferencesApi getPreferencesApi() {
        return this.getApi(PreferencesApi.class);
    }
    
    default ProcessDefinitionsApi getProcessDefinitionApi() {
        return this.getApi(ProcessDefinitionsApi.class);
    }
    
    default ProcessesApi getProcessesApi() {
        return this.getApi(ProcessesApi.class);
    }
	
	default ProbesApi getProbesApi() {
		return this.getApi(ProbesApi.class);
	}
	
	default QueriesApi getQueriesApi() {
		return this.getApi(QueriesApi.class);
	}
	
	default RatingsApi getRatingsApi() {
		return this.getApi(RatingsApi.class);
	}
	
	default RenditionsApi getRenditionsApi() {
		return this.getApi(RenditionsApi.class);
	}
	
	default SearchApi getSearchApi() {
		return this.getApi(SearchApi.class);
	}
	
	default SharedLinksApi getSharedLinksApi() {
		return this.getApi(SharedLinksApi.class);
	}
	
	default SitesApi getSitesApi() {
		return this.getApi(SitesApi.class);
	}
	
	default TagsApi getTagsApi() {
		return this.getApi(TagsApi.class);
	}
    
    default TasksApi getTasksApi() {
        return this.getApi(TasksApi.class);
    }
	
	default TrashcanApi getTrashcanApi() {
		return this.getApi(TrashcanApi.class);
	}
	
	default TypesApi getTypesApi() {
		return this.getApi(TypesApi.class);
	}
	
	default VersionsApi getVersionsApi() {
		return this.getApi(VersionsApi.class);
	}
	
	default V0Api getV0Api() {
		return this.getApi(V0Api.class);
	}
	
	default V0Api getLegacyApi() {
		return this.getV0Api();
	}

}
