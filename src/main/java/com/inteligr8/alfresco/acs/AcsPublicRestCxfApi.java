/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.acs;

import com.inteligr8.alfresco.acs.api.NodesCxfApi;

/**
 * This interface appends Apache CXF implementation specific methods to the
 * JAX-RS API of the ACS Public ReST API.  This is due to a lack of multi-part
 * in the JAX-RS specification.
 * 
 * @author brian@inteligr8.com
 */
public interface AcsPublicRestCxfApi extends AcsPublicRestApi {
	
	default NodesCxfApi getNodesExtApi() {
		return this.getApi(NodesCxfApi.class);
	}

}
